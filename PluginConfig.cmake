#/**********************************************************\ 
#
# Auto-Generated Plugin Configuration file
# for SCard COM
#
#\**********************************************************/

set(PLUGIN_NAME "SCardCOM")
set(PLUGIN_PREFIX "SCC")
set(COMPANY_NAME "cardid.org")

# ActiveX constants:
set(FBTYPELIB_NAME SCardCOMLib)
set(FBTYPELIB_DESC "SCardCOM Type Library")
set(IFBControl_DESC "SCardCOM Control Interface")
set(FBControl_DESC "SCardCOM Control Class")
set(IFBComJavascriptObject_DESC "SCardCOM IComJavascriptObject Interface")
set(FBComJavascriptObject_DESC "SCardCOM ComJavascriptObject Class")
set(IFBComEventSource_DESC "SCardCOM IFBComEventSource Interface")
set(AXVERSION_NUM "1")

# NOTE: THESE GUIDS *MUST* BE UNIQUE TO YOUR PLUGIN/ACTIVEX CONTROL!  YES, ALL OF THEM!
set(FBTYPELIB_GUID 8e45ea49-fa87-571d-8cf8-5432cec57e75)
set(IFBControl_GUID 78db2bf8-e784-5ec4-8577-4f9ac30e5e1f)
set(FBControl_GUID c5a95845-e100-52dd-9862-f81761830ad6)
set(IFBComJavascriptObject_GUID 8593163a-d691-5113-8ece-a29520853c3b)
set(FBComJavascriptObject_GUID 5422cfca-a60a-5be5-9b11-ebea1bcb6d54)
set(IFBComEventSource_GUID 8b64e019-0624-5b51-b496-6fc9a384c556)

# these are the pieces that are relevant to using it from Javascript
set(ACTIVEX_PROGID "cardid.scardcom")
set(MOZILLA_PLUGINID "cardid.org/scardcom")

# strings
set(FBSTRING_CompanyName "plugin.cardid.org")
set(FBSTRING_PluginDescription "Smart Card Browser Plugin")
set(FBSTRING_PLUGIN_VERSION "0.6.4")
set(FBSTRING_LegalCopyright "Copyright 2012 cardid.org")
set(FBSTRING_PluginFileName "np${PLUGIN_NAME}.dll")
set(FBSTRING_ProductName "Smart Card Browser Plugin")
set(FBSTRING_FileExtents "")
set(FBSTRING_PluginName "Smart Card Browser Plugin")
set(FBSTRING_MIMEType "application/x-scardcom")

# Uncomment this next line if you're not planning on your plugin doing
# any drawing:

set (FB_GUI_DISABLED 1)

# Mac plugin settings. If your plugin does not draw, set these all to 0
set(FBMAC_USE_QUICKDRAW 0)
set(FBMAC_USE_CARBON 0)
set(FBMAC_USE_COCOA 0)
set(FBMAC_USE_COREGRAPHICS 0)
set(FBMAC_USE_COREANIMATION 0)
set(FBMAC_USE_INVALIDATINGCOREANIMATION 0)

# If you want to register per-machine on Windows, uncomment this line
#set (FB_ATLREG_MACHINEWIDE 1)
