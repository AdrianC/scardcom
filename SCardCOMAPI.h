/**********************************************************\

  Auto-generated SCardCOMAPI.h

\**********************************************************/

#include <string>
#include <sstream>
#include <stdlib.h>
#include <boost/weak_ptr.hpp>
#include "JSAPIAuto.h"
#include "BrowserHost.h"
#include "SCardCOM.h"
#include "Reader.h"

#ifndef H_SCardCOMAPI
#define H_SCardCOMAPI

FB_FORWARD_PTR(Reader);

class SCardCOMAPI : public FB::JSAPIAuto
{
public:
    ////////////////////////////////////////////////////////////////////////////
    /// @fn SCardCOMAPI::SCardCOMAPI(const SCardCOMPtr& plugin, const FB::BrowserHostPtr host)
    ///
    /// @brief  Constructor for your JSAPI object.
    ///         You should register your methods, properties, and events
    ///         that should be accessible to Javascript from here.
    ///
    /// @see FB::JSAPIAuto::registerMethod
    /// @see FB::JSAPIAuto::registerProperty
    /// @see FB::JSAPIAuto::registerEvent
    ////////////////////////////////////////////////////////////////////////////
    SCardCOMAPI(const SCardCOMPtr& plugin, const FB::BrowserHostPtr& host) :
        m_plugin(plugin), m_host(host)
    {
		// Reader list
		registerProperty("readers",
						 make_property(this,
									   &SCardCOMAPI::get_readers));
        
        // Read-only property
        registerProperty("version",
                         make_property(this,
                                       &SCardCOMAPI::get_version));

		InitializeReaders();
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// @fn SCardCOMAPI::~SCardCOMAPI()
    ///
    /// @brief  Destructor.  Remember that this object will not be released until
    ///         the browser is done with it; this will almost definitely be after
    ///         the plugin is released.
    ///////////////////////////////////////////////////////////////////////////////
    virtual ~SCardCOMAPI() {
		ReleaseReaders();
	};

    SCardCOMPtr getPlugin();

	// Read-only property ${PROPERTY.ident}
	FB::JSObjectPtr get_readers();

    // Read-only property ${PROPERTY.ident}
    std::string get_version();

    // Event helpers
    FB_JSAPI_EVENT(statuschange, 1, (const FB::variant&));

	void InitializeReaders();
	void ReleaseReaders();

protected:
    void threadRun();

private:
    SCardCOMWeakPtr m_plugin;
    FB::BrowserHostPtr m_host;

    std::string m_testString;
	SCARDCONTEXT m_hContext;
	LPTSTR  m_pmszReaders;
#ifdef _WINDOWS
	LPSCARD_READERSTATE m_rgReaderStates;
#else
	LPSCARD_READERSTATE_A m_rgReaderStates;
#endif
	int m_cReaders;
	FB::JSObjectPtr m_readers;
    boost::thread m_thread;
};

#endif // H_SCardCOMAPI

